open Graphics;;
open Affichage;;
open Voronoi;;

(* Type correspondant à l'état actuel du jeu (le joueur est en train de jouer, a gagné, veut quitter le jeu,
   veut afficher une solution au voronoi, veut recommencer sur le même voronoi,
   veut jouer sur un nouveau voronoi ou veut annuler le dernier coup)*)
type state =
    Win
  | Play
  | Quit
  | Solution
  | Restart
  | New_game
  | CancelMove
;;


(*Liste contenant les coups effectués par le joueur, modélisés par 
  l'index de la germe associé à la couleur qu'elle avait avant qu'il joue*)
let listMoves : (int  * color option) list ref = ref [];;



(*************** Choix du joueur effectués en début de partie ****************)

(* Choix de la distance utilisée *)
let choose_dist () =
  draw_choice_dist_message();
  let rec choose () = 
    let eve = wait_next_event[Key_pressed] in
    match eve.key with
      't' | 'T' -> taxicab
    |'e' | 'E' -> euclide
    | _ -> choose()
  in let choice = choose()
     in
     clear_graph();
     choice
;;


(* Choix de la difficulté du voronoi*)
let choose_type_voronoi () = 
  draw_choice_voro_message();
  let rec choose () = 
    let eve = wait_next_event[Key_pressed] in
    match eve.key with
      c when c = '1' 
          || c = '2' 
          || c = '3' -> int_of_char(c) - int_of_char('0')
    | _ -> choose()
  in let choice = voronoiOfDiff (choose()) in
     clear_graph();
     choice;;


(************************* Coup du joueur ***********************)

(*Change l'état de "s" (de type state) en fonction de la touche "c" pressée par le joueur*)
let keytoplay c s = 
  match c with
    'r'-> s := Restart
  | 's'-> s := Solution
  | 'q'-> s := Quit
  | 'n'-> s := New_game
  | '\b'-> s := CancelMove
  |_->()
;;


(*Teste si les coordonnées de "point" se situent dans le voronoi, de taille "dimVoro"*)
let mouseInVoronoi point dimVoro =
  fst point >= 0 &&
  snd point >= 0 &&
  fst point < fst dimVoro &&
  snd point < snd dimVoro
;;


(*Gère la dernière phase d'un coup du joueur : si l'évènement détecté est le relachement
  du bouton de la souris, on met a jour "colors" et on ajoute le coup à "listMoves" ;
  sinon, on rappelle cette fonction (le bouton de la souris est encore enfoncé) *)
let rec moveColor initialColors colors v m col s=
  let eve = wait_next_event [Mouse_motion; Button_up] in
  let point = let mid = getPointMiddle (v.dim)
              in (eve.mouse_x - fst mid, eve.mouse_y - snd mid) in
  if mouseInVoronoi point (v.dim)
  then (
    if button_down() then(
      moveColor initialColors colors v m col s;
    )else (
      let coloredSeed = m.(fst point).(snd point)
      in
      if initialColors.(coloredSeed) = None then (
        listMoves := (coloredSeed, colors.(coloredSeed))::!listMoves;
	colors.(coloredSeed)<-Some(col);
      )
    )
  ) else moveColor initialColors colors v m col s
;;


(*Gère la première partie d'un coup du joueur, lorsqu'il presse le bouton de la souris,
  ou lorsqu'il appuie sur une touche pour recommencer, quitter, etc. 
  Si on ets dans le premier cas, on récupère la couleur de la zone où le joueur a cliqué
  avant d'appeler moveColors, sinon on détermine la touche pressée grâce a keytoPlay *)
let rec chooseColor initialColors colors v m s =
  let eve = wait_next_event[Button_down; Key_pressed] in
  if (eve.keypressed) then keytoplay (eve.key) s
  else 
    let point = let mid = getPointMiddle (v.dim)
                in (eve.mouse_x - fst mid, eve.mouse_y - snd mid)
    in
    if mouseInVoronoi point (v.dim)
    then (
      let col =
        match colors.(m.(fst point).(snd point)) with
          None -> white
        | Some (c)  -> c
      in
      set_color col;
      moveColor initialColors colors v m col s;
    )else chooseColor initialColors colors v m s
;; 


(*Récupère l'action que veut effectuer le joueur lorsqu'il a gagné *)
let end_of_game colors m s=
  let eve = wait_next_event[Key_pressed] in
  keytoplay (eve.key) s;;




