open Graphics;;
open Sat_solver;;

type seed =
  { c : color option ;
    x : int;
    y : int
  };;

type voronoi =
  { dim : int*int ;
    seeds : seed array
  };;


(*Exception levée lorsque le sat solver ne trouve pas de solution à un voronoi*)
exception No_Solution;;


(* Cette liste contient tous les voronois déclarés dans le fichier examples.ml,
   chacun associe a un booleen egal a false tant que le joueur n'a pas joué le voronoi *)
let list_of_voronois : (bool ref * voronoi) list ref = ref [];

module Variables = struct
  type t = (int * color)
  let compare x y = compare x y
end ;;

module Sat = Make(Variables);;


(**************** Ajouter des voronois a la liste de voronois *******************)

(*Ajoute un seul voronoi à la liste *)
let addVoronoiToList v = 
  list_of_voronois := (ref false, v)::!list_of_voronois;;

(*ajoute une liste de voronois a la liste *)
let addVoronoiListToList vlist = 
  let rec addOneVoro l = 
    match l with 
      [] -> ()
    | v::q -> addVoronoiToList v;
      addOneVoro q
  in addOneVoro vlist
;;


(*Choisit un voronoi de difficulte 'diff' choisie par le joueur,
  parmi ceux qu'il n'a pas encore joués, ou retourne un voronoi correspondant 
  à la difficulté choisie s'il les a tous déjà joués*)
let voronoiOfDiff diff =
  let range = 
    match diff with
      1 -> (0, 10)
    | 2 -> (6, 20)
    | 3 -> (20, 100)
    | _ -> (0, 0)
  in
  let rec aux l currIndex indexDefault =  
    match l with
      [] -> snd (List.nth (!list_of_voronois) indexDefault)
    | v::q -> let nbSeeds = Array.length ((snd v).seeds) in 
              if nbSeeds >= fst range &&
                nbSeeds <= snd range
              then if !(fst v) = false
                then (fst v := true;
                      snd v
                ) else
                  if indexDefault = -1 then aux q (currIndex+1) currIndex
                  else aux q (currIndex+1) indexDefault
              else aux q (currIndex+1) indexDefault
  in aux (!list_of_voronois) 0 (-1) 
;; 

(************************* Distances possibles *************************)

let euclide p1 p2 =
  let i = float_of_int (fst p1 - fst p2)
  and j = float_of_int (snd p1 - snd p2)
  in int_of_float (sqrt (i*.i +. j*.j));;

let taxicab p1 p2 =
  abs (fst p1 - fst p2) + abs (snd p1 - snd p2);;


(************************* Fonctions calculant les matrices
 			   de regions et d'adjacence d'un voronoi *************************)

(*Retourne l'index de la germe étant la plus proche du point "pixel"*)
let calculateMin dist pixel v len =
  let rec aux index mini indexMin =
    if (index = len) then indexMin
    else
      let s = (v.seeds.(index).x , v.seeds.(index).y)
      in let distSPixel = dist s pixel
	 in
	 if (min mini distSPixel = mini)
	 then aux (index+1) mini indexMin
	 else aux (index+1) distSPixel index
  in let maxPossible = dist (0,0) (fst v.dim, snd v.dim)
     in aux 0 maxPossible 0
;;


(*Calcule la matrice de régions du voronoi "v" avec la distance "dist"*)
let regions_voronoi dist v =
  let dimX = fst (v.dim) and dimY = snd (v.dim)
  and length = Array.length v.seeds
  in let m = Array.make_matrix dimX dimY 0
     in let rec minimalSeed i j =
	  if i < dimX then (
	    if j < dimY then (
	      m.(i).(j) <- calculateMin dist (i,j) v length;
	      minimalSeed i (j+1);
	    )
	    else minimalSeed (i+1) 0
	  )
	in minimalSeed 0 0 ;
	m
;;



(*Teste si les germes d'index h et k sont adjacentes ou non*)
let isAdja h k dimX dimY reg =
  let rec aux i j =
    if i < dimX then
      if j < dimY then
	if (i-1 >= 0 && i+1 < dimX && j-1 >= 0 && j+1 < dimY)
	  && reg.(i).(j) = h &&
	  (reg.(i-1).(j) = k || reg.(i+1).(j) = k ||
	   reg.(i).(j-1) = k || reg.(i).(j+1) = k )
	then true
	else aux i (j+1)
      else aux (i+1) 0
    else false
  in aux 0 0;;


(*Calcule la matrice d'adjacence du voonoi v*)
let adjacences_voronoi v reg =
  let n = Array.length v.seeds in
  let b = Array.make_matrix n n false in
  let dimX = fst (v.dim) and dimY = snd (v.dim) in
  let rec aux h k =
    if h < n then (
      if k < n then (
	if h > k then b.(h).(k) <- b.(k).(h)
	else if h = k then b.(h).(k) <- true
	else
	  b.(h).(k) <- isAdja h k dimX dimY reg;
	aux h (k+1);
      ) else aux (h+1) 0
    ) else ()
  in aux 0 0; b ;;


(************************* Construction de la FNC *************************)

(*Renvoie un tableau correspondant au champ "c : color option" de chaque germe*)
let getPartialColors v =
  let t = Array.make (Array.length v.seeds) None
  in
  for i = 0 to Array.length v.seeds-1 do
    t.(i) <- v.seeds.(i).c
  done;
  t;
;;


(*Renvoie un tableau constitué des couleurs utilisée dans la définition d'un voronoi *)
let getSomeColorsArray partialColors nbSeeds = 
  let rec aux i l = 
    if i = nbSeeds then l
    else match partialColors.(i) with
      Some(c) when not (List.exists (fun el -> el = c) l) -> aux (i+1) (c::l)
    | _ -> aux (i+1) l
  in Array.of_list (aux 0 [])
;;


(*Ajoute à la fnc les clauses modélisant l'existence d'une couleur pour chaque germe*)
let existence fnc colors nbCol nbSeeds =
  let rec aux i col sl =
    if i < nbSeeds then
      if col < nbCol then
        aux i (col+1) ((true, (i, colors.(col)))::sl)
      else (
        fnc := sl::!fnc; 
        aux (i+1) 0 [];
      )
  in aux 0 0 []
;;

(*Ajoute à la fnc les clauses modélisant l'unicité d'une couleur pour chaque germe*)
let unicity fnc colors nbCol nbSeeds =
  let rec aux i fstCol sndCol =
    if i < nbSeeds then
      if fstCol < nbCol then
	if sndCol < nbCol then (
          fnc := [(false, (i, colors.(fstCol)));(false, (i, colors.(sndCol)))]::!fnc;
	  aux i fstCol (sndCol+1)
	) else aux i (fstCol+1) (fstCol+2)
      else aux (i+1) 0 1
  in aux 0 0 1
;;

(*Ajoute à la fnc les clauses modélisant les conditions d'adjacence:
  deux germes adjacentes ne peuvent pas avoir la même couleur*)
let adjacency fnc colors nbCol nbSeeds adja =
  let rec aux i j col =
    if i < nbSeeds then
      if j < nbSeeds then
        if col < nbCol then
	  if adja.(i).(j) = true then (
	    fnc := [(false, (i,colors.(col)));(false, (j,colors.(col)))]::!fnc ;
            aux i j (col+1); )
	  else aux i j (col+1)
        else aux i (j+1) 0
      else aux (i+1) (i+2) 0
  in aux 0 1 0
;;

(*Ajoute à la fnc les clauses modélisant le fait qu'une germe
  doit avoir une couleur spécifique si la définition du voronoi lui en donne une*)
let fixed_colors_constraints fnc partialColors nbSeeds=
  let rec aux i =
    if i < nbSeeds then
      match partialColors.(i) with
	None -> aux (i+1)
      | Some(c) -> fnc:= [true, (i, c)]::!fnc; aux (i+1)
  in aux 0
;;


(*Construit la fnc complète*)
let produce_constraints partialColors adja =
  let nbSeeds = Array.length partialColors
  in let colors = getSomeColorsArray partialColors nbSeeds
     in let nbSomeCol = Array.length colors
	in let fnc = ref [] 
	   in  unicity fnc colors nbSomeCol nbSeeds;
	   existence fnc colors nbSomeCol nbSeeds;
	   adjacency fnc colors nbSomeCol nbSeeds adja;
	   fixed_colors_constraints fnc partialColors nbSeeds;
	   !fnc;
;;

(********************************** Solution d'un voronoi ************************)

(* Renvoie le tableau "partialColors" rempli avec une solution "sol" donnée par le sat solver *)
let coloration_of_valuation partialColors sol =
  let rec fillTab l =
    match l with
      [] -> ()
    | a::q -> ( match a with
      (true, ( seed, col)) -> partialColors.(seed) <- Some(col);
	fillTab q
      | _ -> fillTab q)
  in fillTab sol;
  partialColors
;;


(* Applique le sat solver sur une fnc : si il existe une solution, on renvoie 
   le tableau "partialColors" rempli avec celle-ci, sinon on lève une exception No_Solution*)
let getSolution dist v partialColors reg adja =
  let fnc = Sat.solve (produce_constraints partialColors adja) in
  match fnc with
    None -> raise No_Solution
  | Some(l) -> coloration_of_valuation partialColors l
;;

