open Graphics;;

(* Largeur du panneau contenant la liste des commandes disponibles*)
let right_pane_width = 200;;


(* Dessine le panneau droit, avec la description des commandes
pouvant être effectuées par le joueur*)
let draw_right_pane () =
  set_color 0xABABAB;
  fill_rect ((size_x()-right_pane_width)) 0 right_pane_width (size_y());
  set_color black;
  moveto (size_x()-right_pane_width) 700;
  draw_string("            OPTIONS : ");
  moveto (size_x()-right_pane_width) 630;
  draw_string("Annuler le dernier coup :  ' <- '");
  moveto (size_x()-right_pane_width) 590;
  draw_string("Recommencer :  ' r '");
  moveto (size_x()-right_pane_width) 550;
  draw_string("Afficher la solution ' s '");
  moveto (size_x()-right_pane_width) 510;
  draw_string("Nouvelle partie : ' n '");
  moveto (size_x()-right_pane_width) 470;
  draw_string("Quitter : ' q '")
;;

(*Retourne le coin superieur gauche du voronoi une fois place au milieu de l'ecran*)
let getPointMiddle dimVoro = 
  ((size_x() - right_pane_width - fst dimVoro)/2, (size_y() - snd dimVoro)/2)
;;


(*Teste si le point (i,j) doit être dessine en noir ou non
(frontiere de deux regions)*)
let isItBlack m i j =
  try (
    let s = m.(i).(j) in
    if(m.(i+1).(j)<>s
       || m.(i-1).(j)<>s
       || m.(i).(j+1)<>s
       || m.(i).(j-1)<>s)
    then true 
    else false;
  ) with Invalid_argument(str)->true;;


(*Dessine un voronoi à l'écran*)
let draw pointMid colors m =
  auto_synchronize false;
  draw_right_pane();
  for i = 0 to Array.length m -1 do
    for j = 0 to Array.length m.(0)-1 do
      if isItBlack m i j
      then set_color black
      else
	( match (colors.(m.(i).(j))) with
	  None -> set_color white
	| Some (c) -> set_color c );
      plot (i+fst pointMid) (j+snd pointMid);
    done;
  done;
  synchronize();;


(*Affiche les choix disponibles à l'utilisateur pour le type de distance utilisé*)
let draw_choice_dist_message ()=
  auto_synchronize false;
  set_color black;
  moveto 400 460;
  draw_string "Choisissez le type de distance utilise:";
  moveto 400 430;
  draw_string "Pour une distance taxicab, appuyer sur t";
  moveto 400 400;
  draw_string "Pour une distance euclide, appuyer sur e";
  synchronize ();;


(*Affiche les choix disponibles à l'utilisateur pour la difficulté du voronoi*)
let draw_choice_voro_message ()=
  auto_synchronize false;
  set_color black;
  moveto 400 400;
  draw_string "Choisissez votre difficulte:";
  moveto 200 360;
  draw_string "1 : Facile (<10 zones)             2 : Normal (10-20 zones)              3 : Difficile (>20zones)";
  synchronize ();;


(*Affiche un message de victoire à l'écran*)
let draw_win_message pointMid colors m =
    draw pointMid colors m;
    moveto (size_x()-right_pane_width) 380;
    draw_string "      VOUS AVEZ GAGNE ! ";
    synchronize();;
