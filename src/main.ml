
open Graphics;;
open Voronoi;;
open Examples;;
open Affichage;;
open Interaction;;


(*Appelée à chaque coup du joueur, teste si le joueur a mis une couleur
  sur toutes les zones du voronoi*)
let isFill colors =
  let rec aux i =
    if (i=Array.length colors)
    then true
    else
      match colors.(i) with
        None -> false
      | Some(c) ->  if(c=white) then false else aux (i+1)
  in aux 0
;;


(*Appelée si le voronoi n'a plus de zones sans couleur,
  teste s'il s'agit d'une configuration gagnante ou non*)
let win colors v m =
  let b = ref true in
  for i = 0 to Array.length m -1 do
    for j = 0 to Array.length m.(0)-1 do
      if (m.(i).(j)=true && colors.(i)=colors.(j) && i<>j)
      then b:=false
    done;
  done;
  !b
;;


(*Affiche une solution du voronoi donnée par le sat_solver si elle existe*)
let show_solution dist v initialCol reg adja =
  try 
    let solution = getSolution dist v initialCol reg adja
    and pointMid = getPointMiddle (v.dim)
    in draw pointMid solution reg;
    true;
  with No_Solution ->  print_endline "Erreur : aucune solution trouvée";
    false
;;


(********************************* Moteur du programme **************************************)

(*Retourne les donnees necessaires au bon deroulement d'une partie, c'est-a-dire la distance
  et le voronoi choisis par le joueur, ses matrices d'adjacence et de regions, ainsi que deux tableaux
  remplis avec les couleurs partielles du voronoi (le premier sera rempli au fur et a mesure du jeu,
  mais le deuxieme ne sera pas modifi : il sert seulement a savoir quelles zones etaient initialement coloriees)*)
let getMainParam () = 
	let dist = choose_dist() in
    let v = choose_type_voronoi() in
    let reg = regions_voronoi dist v
    and colors = getPartialColors v
    and initialCol = getPartialColors v in
    let adja = adjacences_voronoi v reg in
    (dist, v, reg, colors, initialCol, adja)
;;


(*Fonctions mutuellement récursives : 
  - lookstate effectue les actions corespondantes à l'état de "s" (afficher une solution, recomencer, etc)
  - playloop est la boucle principale du programme ; on sort de la boucle si "s" est dans un autre état que "Play", 
  et appelle alors lookstate pour effectuer l'action *)
let rec lookState s dist v colors initialCol reg adja =
  match !s with
    Solution ->
      if (show_solution dist v initialCol reg adja) = false then s := Quit
      else end_of_game colors reg s;
      lookState s dist v colors (getPartialColors v) reg adja;

  | Restart->
     clear_graph();
    listMoves := [];
    let colors = getPartialColors v in
    playLoop (ref Play) dist v colors initialCol reg adja;

  | New_game -> 
     clear_graph();
    listMoves := [];
    let (dist, v, reg, colors, initialCol, adja) = getMainParam()
    in
    playLoop (ref Play) dist v colors initialCol reg adja;

  | Win ->
     listMoves := [];
    end_of_game colors reg s;
    lookState s dist v colors initialCol reg adja;

  | CancelMove ->
     s:=Play;
    clear_graph();
    if List.length (!listMoves) <> 0 then (
      let (seed, color) = List.hd (!listMoves) in
      colors.(seed) <- color;
      listMoves := List.tl (!listMoves);
    );
    playLoop (ref Play) dist v colors initialCol reg adja;

  | Quit -> ();
  | _ -> ()

and

    playLoop state dist v colors initialCol reg adja =
  let pointMid = getPointMiddle (v.dim) in
  while !state = Play do
    draw pointMid colors reg;
    chooseColor initialCol colors v reg state;
    if (isFill colors = true && win colors v adja = true)
    then (
      draw_win_message pointMid colors reg;
      state := Win
    )
  done;
  lookState state dist v colors initialCol reg adja;;


(* Fonction principale du programme *)
let main() = 
  addVoronoiListToList list;
  open_graph " 1000x800";
  let (dist, v, reg, colors, initialCol, adja) = getMainParam()
  in
  playLoop (ref Play) dist v colors initialCol reg adja;
  close_graph();
;;


main();;


